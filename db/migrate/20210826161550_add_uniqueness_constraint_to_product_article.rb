class AddUniquenessConstraintToProductArticle < ActiveRecord::Migration[6.1]
  def change
    add_index :product_articles, [:product_id, :article_id], unique: true, name: 'uniq_reference_article_per_product'
  end
end
