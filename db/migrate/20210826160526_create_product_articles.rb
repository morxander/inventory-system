class CreateProductArticles < ActiveRecord::Migration[6.1]
  def change
    create_table :product_articles do |t|
      t.references :product, null: false, foreign_key: true
      t.references :article, null: false, foreign_key: true
      t.integer :amount

      t.timestamps
    end
  end
end
