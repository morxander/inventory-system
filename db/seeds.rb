# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Article.create([
  { id: 1, name: "leg", stock: 12 },
  { id: 2, name: "screw", stock: 17 },
  { id: 3, name: "seat", stock: 2 },
  { id: 4, name: "table top", stock: 1 },
])

Product.create([
  { id: 1, name: "Dining Chair" },
  { id: 2, name: "Dinning Table" },
])

ProductArticle.create([
  {product_id: 1, article_id: 1, amount: 4},
  {product_id: 1, article_id: 2, amount: 8},
  {product_id: 1, article_id: 3, amount: 1},
  {product_id: 2, article_id: 1, amount: 4},
  {product_id: 2, article_id: 2, amount: 8},
  {product_id: 2, article_id: 4, amount: 1},
])