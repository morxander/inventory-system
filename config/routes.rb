Rails.application.routes.draw do
  root 'articles#index'
  resources :articles
  get '/bulk_upload/articles', to: 'articles#bulk_upload'
  post '/bulk_upload/articles', to: 'articles#bulk_upload_process'
  resources :products
  post '/products/:id/sell', to: 'products#sell'
  get '/bulk_upload/products', to: 'products#bulk_upload'
  post '/bulk_upload/products', to: 'products#bulk_upload_process'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
