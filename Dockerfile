FROM ruby:2.7.1

RUN apt update -qq && apt install -y build-essential libpq-dev nodejs
RUN mkdir /app
WORKDIR /app 
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
RUN bundle install
ADD . /app

ENTRYPOINT ["sh", "./startup.sh"]