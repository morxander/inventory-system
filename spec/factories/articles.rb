# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    name { Faker::Name.unique.name }
    stock { Faker::Number.number(digits: 3) }
  end
end
