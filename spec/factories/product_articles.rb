# frozen_string_literal: true

FactoryBot.define do
  factory :product_article do
    sequence(:id)
    sequence(:product_id)
    sequence(:article_id)
    amount { Faker::Number.number(digits: 3) }
  end
end
