# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'Products', type: :request do
  describe 'GET /index' do
    let(:product1) { FactoryBot.create(:product, id: 1) }
    let(:product2) { FactoryBot.create(:product, id: 2) }
    it 'returns http success' do
      get '/products/'
      expect(response).to have_http_status(:success)
      expect(assigns(:products)).to eq(Product.all)
    end
  end

  describe 'GET /new' do
    it 'returns http success' do
      get '/products/'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /create' do
    let!(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    it 'returns http success' do
      params = { product:
        {
          name: 'Test',
          product_articles_attributes: {
            '1': {
              article_id: article.id,
              amount: 2
            }
          }
        } }
      post '/products/', params: params
      expect(response).to have_http_status(302)
      product = Product.first
      expect(product.name).to eq('Test')
      expect(product.product_articles.first.article_id).to eq(article.id)
    end
  end

  describe 'GET /show' do
    let!(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    let!(:product) { FactoryBot.create(:product, id: 1) }
    let!(:product_article) { FactoryBot.create(:product_article, product_id: 1, article_id: 1, amount: 1) }
    it 'returns http success' do
      get "/products/#{product.id}"
      expect(response).to have_http_status(:success)
      expect(assigns(:articles)).to eq(Article.all)
      expect(assigns(:product)).to eq(product)
    end
  end

  describe 'GET /edit' do
    let!(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    let!(:product) { FactoryBot.create(:product, id: 1) }
    let!(:product_article) { FactoryBot.create(:product_article, product_id: 1, article_id: 1, amount: 1) }
    it 'returns http success' do
      get "/products/#{product.id}"
      expect(response).to have_http_status(:success)
      expect(assigns(:articles)).to eq(Article.all)
      expect(assigns(:product)).to eq(product)
    end
  end

  describe 'GET /update' do
    let!(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    let!(:product) { FactoryBot.create(:product, id: 1) }
    it 'returns http success' do
      params = { product:
        {
          name: 'Test',
          product_articles_attributes: {
            '1': {
              article_id: article.id,
              amount: 2
            }
          }
        } }
      put "/products/#{product.id}", params: params
      expect(response).to have_http_status(302)
      product = Product.first.reload
      expect(product.name).to eq('Test')
      expect(product.product_articles.first.article_id).to eq(article.id)
      expect(product.product_articles.first.amount).to eq(2)
    end
  end

  describe 'GET /destroy' do
    let!(:product1) { FactoryBot.create(:product, id: 1) }
    let!(:product2) { FactoryBot.create(:product, id: 2) }
    it 'returns http success' do
      delete "/products/#{product1.id}"
      expect(response).to have_http_status(302)
      expect(Product.find_by(id: product1.id)).to be_nil
    end
  end
end
# rubocop:enable Metrics/BlockLength
