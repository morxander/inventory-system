# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'Articles', type: :request do
  describe 'GET /index' do
    let(:article1) { FactoryBot.create(:article, id: 1, stock: 3) }
    let(:article2) { FactoryBot.create(:article, id: 2, stock: 5) }
    it 'returns http success' do
      get '/articles/'
      expect(response).to have_http_status(:success)
      expect(assigns(:articles)).to eq(Article.all)
    end
  end

  describe 'GET /new' do
    it 'returns http success' do
      get '/articles/new'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /create' do
    it 'returns http success' do
      post '/articles/', params: { article: { name: 'Test name', stock: 10 } }
      expect(response).to have_http_status(302)
      expect(Article.first.name).to eq('Test name')
    end
  end

  describe 'GET /show' do
    let(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    it 'returns http success' do
      get "/articles/#{article.id}"
      expect(response).to have_http_status(:success)
      expect(assigns(:article)).to eq(article)
    end
  end

  describe 'GET /edit' do
    let(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    it 'returns http success' do
      get "/articles/#{article.id}"
      expect(response).to have_http_status(:success)
      expect(assigns(:article)).to eq(article)
    end
  end

  describe 'GET /update' do
    let(:article) { FactoryBot.create(:article, id: 1, stock: 3) }
    it 'returns http success' do
      put "/articles/#{article.id}", params: { article: { name: 'Test name', stock: 10 } }
      expect(response).to have_http_status(302)
      expect(article.reload.name).to eq('Test name')
      expect(article.reload.stock).to eq(10)
    end
  end

  describe 'GET /destroy' do
    let(:article1) { FactoryBot.create(:article, id: 1, stock: 3) }
    let(:article2) { FactoryBot.create(:article, id: 2, stock: 3) }
    it 'returns http success' do
      delete "/articles/#{article1.id}"
      expect(response).to have_http_status(302)
      expect(Article.find_by(id: article1.id)).to be_nil
    end
  end
end
# rubocop:enable Metrics/BlockLength
