# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  context '#quantity' do
    describe 'when the articles are enough' do
      let!(:article) { FactoryBot.create(:article, stock: 3) }
      let!(:product) { FactoryBot.create(:product) }
      let!(:product_article) do
        FactoryBot.create(:product_article, product_id: product.id, article_id: article.id, amount: 1)
      end

      it 'should return the quantity' do
        expect(product.quantity).to eq 3
      end
    end

    describe 'when the articles are not enough' do
      let!(:article) { FactoryBot.create(:article, stock: 0) }
      let!(:product) { FactoryBot.create(:product) }
      let!(:product_article) do
        FactoryBot.create(:product_article, product_id: product.id, article_id: article.id, amount: 1)
      end

      it 'should return the quantity' do
        expect(product.quantity).to eq 0
      end
    end
  end
end
