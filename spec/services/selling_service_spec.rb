# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe Products::SellingService do
  let!(:instance) { described_class.new(product.id, 2) }

  context 'When the DB is not locked' do
    let!(:article1) { FactoryBot.create(:article, id: 1, stock: 3) }
    let!(:article2) { FactoryBot.create(:article, id: 2, stock: 3) }
    let!(:product) { FactoryBot.create(:product) }
    let!(:product_article1) do
      FactoryBot.create(:product_article,
                        product_id: product.id,
                        article_id: article1.id,
                        amount: 1)
    end
    let!(:product_article2) do
      FactoryBot.create(:product_article,
                        product_id: product.id,
                        article_id: article2.id,
                        amount: 1)
    end

    describe 'When the stock is enough' do
      subject { instance.process }

      it 'Decrease the stock accordingly' do
        expect(subject).to be_truthy
        expect(article1.reload.stock).to eq 1
        expect(article2.reload.stock).to eq 1
      end
    end

    describe 'When the stock is not enough' do
      let(:article2) { FactoryBot.create(:article, id: 2, stock: 0) }

      subject { instance.process }

      it 'Decrease the stock accordingly' do
        expect { subject }.to raise_error(Products::SellingService::NotEnoughStockError)
        expect(article1.reload.stock).to eq 3
        expect(article2.reload.stock).to eq 0
      end
    end
  end

  context 'When the DB is locked', skip: true do
    # TODO: To be implemented
  end
end
# rubocop:enable Metrics/BlockLength
