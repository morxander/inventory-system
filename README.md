# Inventory system

This is an example project for an Inventory System using `Ruby on Rails`, `MySQL`, and `Redis`.

# Table of Contents

[TOC]


## System dependencies

- Ruby v2.7.1
- MySQL v5.7
- Redis v5.2.6

## DB Schema

![DB](./assets/DB.png)

## How to run the project

### Dokcer-compose

If you have `docker-compose` installed on your system you can just execute the following:

```sh
$ docker-compose build
$ docker-compose up
```

Then you can visit the project on: [https://127.0.0.1:3001](https://127.0.0.1:3001)

### Manually

Make sure to install the system requirements and export the following environment variables:

- DB_HOST
- DB_USER
- DB_NAME
- DB_PASSWORD
- REDIS_URL

Then install the dependencies:

```sh
$ bundle install
$ yarn install
```

Prepare the DB:

```sh
$ bundle exec rake db:create db:migrate
```

Compile the assets:

```sh
$ bundle exec rake assets:precompile
```

[Optional] Seed the DB if you need some test data

```sh
$ bundle exec rake db:seed
```

And now the app is ready to start

```sh
$ bundle exec rails s # to start the web server
$ bundle exec sidekiq # to start sidekiq
```

Then you can visit the project on: [https://127.0.0.1:3000](https://127.0.0.1:3000)

## How to run the test suite

Basically you need to use `rspec` :

```sh
$ rspec
```

## How to run code lint checker

Trigger `rubocop` and it should scan the whole project:

```sh
$ rubocop
```

## Code walk-through

If you're new to rails and want to quickly get how the project works then here you're some useful.

### Controllers and views

You can find the controllers in `./app/controllers/` and the associated views are in `./app/views/`.

- `ArticlesController`: This is the CRUD controller for the Articles.
- `ProductsController`: This is the CRUD controller for the Products.

### Service objects

These are some extra business logic that don't fall under the Rails MVC.

- `Products::SellingService`: This service is responsible for selling the products and make sure there is enough stock before passing the sale.

### Workers

These are background workers for the bulk upload feature (bulk upload articles/products). They're `async` workers since we might parse huge `JSON` files so it has to be done `async` and insert the data in `batches` into the DB.
You can find them under `./app/workers/`.

- `UploadArticlesWorker`: This worker taskes the `articles` as `JSON` then inserts every `100` articles into the DB as a batch.
- `UploadProductsWorker`: This workers takes the `products` and `product_articles` as `JSON` then inserts every `100` products into the DB as a batch.

### Configs

- `./config/database.yml`
- `./config/route.rb`
- `./config/initializers/sidekiq.rb`

## TODOs

Here are some improvements to be considered in the future:

- Introduce an `Authentication` and `Authorization` system.
- Add an audit system to record who did what.
- Generate an `Invoice` after the sale.
- Ditch the `Rails views`, expose an `API` and move the frontend to `ReactJS` or any other FE framework.
- Add validation for the `workers`.
