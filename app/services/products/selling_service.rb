# frozen_string_literal: true

module Products
  class SellingService
    class NotEnoughStockError < StandardError; end

    attr_reader :product, :quantity

    def initialize(product_id, quantity)
      @product = Product.find_by(id: product_id)
      @quantity = quantity
    end

    # The following methods fetches the product articles using
    # SELECT FOR UPDATE to make sure the article records will be
    # locked during the selling process to avoid any race condition
    # If the selling went through it should return true otherwise
    # It will raise NotEnoughStockError
    def process
      product_articles = product.product_articles
      article_amounts = {}
      product_articles.map { |p_a| article_amounts[p_a.article_id] = p_a.amount }
      ApplicationRecord.transaction do
        Article.where(id: article_amounts.keys).lock!.map do |article|
          stock_after_sale = article.stock - (article_amounts[article.id] * quantity)
          raise NotEnoughStockError.new("Not enough stock of #{article.name}") if stock_after_sale < 1

          article.stock = stock_after_sale
          article.save
        end
      end
    end
    true
  end
end
