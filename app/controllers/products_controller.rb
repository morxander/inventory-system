# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    @articles = Article.all
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:notice] = 'The Product has been added!'
      redirect_to product_path(@product.id)
    else
      flash[:error] = 'Failed to add the Product'
      render :new
    end
  end

  def show
    @product = Product.where(id: params[:id]).includes(:product_articles).strict_loading.first
    @articles = Article.all
  end

  def edit
    @product = Product.where(id: params[:id]).includes(:product_articles).strict_loading.first
    @articles = Article.all
  end

  def update
    @product = Product.find_by(id: params[:id])
    if @product.update(product_params)
      flash[:notice] = 'The Product has been updated!'
      redirect_to product_path(@product.id)
    else
      flash[:error] = 'Failed to updates the Product'
      render :new
    end
  end

  def destroy
    @product = Product.find_by(id: params[:id])
    if @product.destroy
      flash[:notice] = 'The Product has been deleted!'
      redirect_to products_path
    else
      flash[:error] = 'Failed to delete this Product!'
      render :destroy
    end
  end

  def sell
    quantity = params[:product][:quantity].to_i
    selling_service = Products::SellingService.new(params[:id], quantity)
    begin
      selling_service.process
      flash[:notice] = 'The Product has been sold'
      redirect_to products_path
    rescue Products::SellingService::NotEnoughStockError => e
      flash[:error] = e.message
      redirect_to products_path
    end
  end

  def bulk_upload; end

  def bulk_upload_process
    json_file_path = params[:products][:json_file].tempfile.path
    file = File.open json_file_path
    products_json = JSON.parse file.read
    UploadProductsWorker.perform_async(products_json: products_json)
    flash[:notice] = 'The Products json file is being processed'
    redirect_to products_path
  end

  private

  def product_params
    params.require(:product).permit(:name, product_articles_attributes: %i[id product_id article_id amount _destroy])
  end
end
