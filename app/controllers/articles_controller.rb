# frozen_string_literal: true

class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    if @article.save
      flash[:notice] = 'The Article has been added!'
      redirect_to article_path(@article.id)
    else
      flash[:error] = 'Failed to add the Article'
      render :new
    end
  end

  def show
    @article = Article.find_by(id: params[:id])
  end

  def edit
    @article = Article.find_by(id: params[:id])
  end

  def update
    @article = Article.find_by(id: params[:id])
    if @article.update(article_params)
      flash[:notice] = 'The Article has been updated!'
      redirect_to article_path(@article.id)
    else
      flash[:error] = 'Failed to updates the Article'
      render :new
    end
  end

  def destroy
    @article = Article.find_by(id: params[:id])
    if @article.destroy
      flash[:notice] = 'The Article has been deleted!'
      redirect_to articles_path
    else
      flash[:error] = 'Failed to delete this Article!'
      render :destroy
    end
  end

  def bulk_upload; end

  def bulk_upload_process
    json_file_path = params[:articles][:json_file].tempfile.path
    file = File.open json_file_path
    articles_json = JSON.parse file.read
    UploadArticlesWorker.perform_async(articles_json: articles_json)
    flash[:notice] = 'The Article json file is being processed'
    redirect_to articles_path
  end

  private

  def article_params
    params.require(:article).permit(:name, :stock)
  end
end
