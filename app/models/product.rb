# frozen_string_literal: true

class Product < ApplicationRecord
  validates :name, presence: true
  has_many :product_articles
  accepts_nested_attributes_for :product_articles, allow_destroy: true, reject_if: :all_blank
  before_destroy :delete_product_articles

  # This method returns the maximum quantity of the product
  # that we can sell depending on the product articles stock
  # so the number is not exactly accurate since these articles
  # are shared and can be used for other products
  def quantity
    quantity = nil
    product_articles.each do |product_article|
      article = Article.find_by(id: product_article.article_id)
      max_items = (article.stock / product_article.amount).round.to_i
      quantity = max_items if quantity.nil? || max_items < quantity
    end
    quantity.to_i
  end

  private

  def delete_product_articles
    ProductArticle.where(product_id: id).delete_all
  end
end
