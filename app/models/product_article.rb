# frozen_string_literal: true

class ProductArticle < ApplicationRecord
  belongs_to :product
  belongs_to :article
  validates :amount, presence: true
end
