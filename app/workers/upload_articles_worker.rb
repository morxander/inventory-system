# frozen_string_literal: true

class UploadArticlesWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  # This method takes the articles json object
  # and insert it in bulk into the DB
  def perform(*args)
    articles_json = args.first['articles_json']
    articles_json['inventory'].each_slice(100) do |batch|
      articles_array = []
      batch.each do |article|
        article = { id: article['art_id'],
                    name: article['name'],
                    stock: article['stock'],
                    created_at: Time.now,
                    updated_at: Time.now }
        articles_array << article
      end
      Article.insert_all(articles_array)
    end
  end
end
