# frozen_string_literal: true

class UploadProductsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  # This method takes the products json object
  # and insert it in bulk into the DB
  def perform(*args)
    products_json = args.first['products_json']
    products_json['products'].each_slice(100) do |batch|
      Product.transaction do
        batch.each do |product|
          product_item = { name: product['name'], product_articles_attributes: [] }
          product['contain_articles'].each do |product_article|
            product_article_item = { article_id: product_article['art_id'], amount: product_article['amount_of'] }
            product_item[:product_articles_attributes] << product_article_item
          end
          Product.create(product_item)
        end
      end
    end
  end
end
